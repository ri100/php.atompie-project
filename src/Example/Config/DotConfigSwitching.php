<?php
namespace Example\Config;

use AtomPie\Web\Boundary\IAmEnvironment;

trait DotConfigSwitching
{
    private static function getDotConfig() {
        return is_file(__DIR__.'/.config')
            ? file_get_contents(__DIR__.'/.config')
            : null;
    }

    public static function __build(IAmEnvironment $oEnv) {
        $sLocalConfigClass = self::getDotConfig();
        if($sLocalConfigClass === null) {
            return new Production($oEnv->getEnv());
        }

        if (!class_exists($sLocalConfigClass)) {
            throw new \Exception(
                sprintf(
                    'Class [%s] does not exist. Config must provide ApplicationConfig class that exists.',
                    $sLocalConfigClass
                )
            );
        }

        return new $sLocalConfigClass($oEnv->getEnv());
    }
}