<?php
namespace Example\EndPoint {

    use AtomPie\AnnotationTag\Client;
    use AtomPie\AnnotationTag\EndPoint;
    use Example\Config\Production;
    use Example\EndPoint\Param\UserName;

    class Hello
    {

        /**
         * @Client(Accept="text/html", Method="GET", Type="WebRequest")
         * @EndPoint(ContentType="text/html")
         * @param UserName $Name
         * @param Production $config
         * @return string
         */
        public function __default(UserName $Name, Production $config)
        {
            return 'Hellos ' . $Name->getValue().' Config:'.$config->mysqlHost;
        }

    }

}